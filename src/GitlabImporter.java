import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GitlabImporter {

	public static final Gson gson = new Gson();

	public static final String ISSUE_TAG = "***ISSUE_ID***";
	public static final String PROJECT_TAG = "***PROJECT_ID***";
	public static final String ISSUE_URL = "https://gitlab.com/api/v3/projects/" + PROJECT_TAG + "/issues";
	public static final String COMMENT_URL = ISSUE_URL + "/" + ISSUE_TAG + "/notes";
	
	public static int addIssue(Issue issue, String token) throws Exception {
		String issueUrl = ISSUE_URL.replace(PROJECT_TAG, String.valueOf(issue.getProjectId()));
		JsonObject response = post(issueUrl, gson.toJson(issue), token);
		
		if (response.get("id") != null) {
			issue.setIssueIds(response.get("id").getAsInt());
		} else {
			throw new Exception(response.toString());
		}
		System.out.println("Created issue " + issue.issue_id);
		
		//Commit comments
		String commentUrl = COMMENT_URL.replace(PROJECT_TAG, String.valueOf(issue.getProjectId())).replace(ISSUE_TAG, String.valueOf(issue.issue_id));
		int size = issue.getComments().size();
		int i = 0;
		for (Issue.Comment c : issue.getComments()) {
			i++;
			System.out.println(post(commentUrl, gson.toJson(c), token));
			System.out.println("Created comment " + i + "/" + size + " on issue " + issue.issue_id);
		}

		return issue.issue_id;
	}

	private static HttpClient client = HttpClientBuilder.create().build();
	private static JsonObject post(String url, String json, String token) throws Exception {
		HttpPost request = new HttpPost(url);
		request.setHeader("PRIVATE-TOKEN", token);
		request.setHeader("Content-Type", "application/json");
		
		StringEntity params = new StringEntity(json);
		params.setContentType("application/json");
		params.setContentEncoding("utf-8");
		params.setChunked(true);
		request.setEntity(params);
		
		HttpResponse response = client.execute(request);
		return gson.fromJson(new InputStreamReader(response.getEntity().getContent()), JsonElement.class).getAsJsonObject();
	}
}
