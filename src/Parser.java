import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Parser implements Iterable<List<String>>, Iterator<List<String>> {

	public static final char DEFAULT_DELIM = ',';

	public static final String N = "\n";
	public static final String Q = "\"";
	public static final String QQ = Q + Q;

	public static final int Qp = Q.codePointAt(0);

	private final int delimiterP;

	private final BufferedReader in;
	private boolean done = false;
	private int lineCount = 0;

	private List<String> next;

	public Parser(String file) {
		this(file, DEFAULT_DELIM);
	}

	public Parser(String file, char delimiter) {
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
		} catch (Exception e) {
			throw new IllegalArgumentException(e.getCause());
		}

		delimiterP = (int) delimiter;
		next();
	}

	private Parser(String file, char delimiter, Void readString) {
		in = new BufferedReader(new StringReader(file));
		delimiterP = ("" + delimiter).codePointAt(0);
		next();
	}


	/** Get the next row of values from this parser */
	@Override
	public List<String> next() {
		if (done) return null;

		List<String> prev = next;
		next = new ArrayList<String>();

		StringBuilder build = new StringBuilder();
		boolean quote = false;

		//0 = outside quote, 1 = inside, 2 = seen inside quote, 3 = seen double
		int quoteCount = 0;

		try {
			while (true) {
				String line = in.readLine();
				if (line == null) break;
				lineCount++;

				//Iterate through characters
				final int length = line.length();
				int offset = 0;
				while (offset < length) {
					//Manage UTF-8 character codepoint and current index
					final int codePoint = line.codePointAt(offset);
					offset += Character.charCount(codePoint);

					//Quote - only operates when quote immediately follows comma for fault tolerance
					if (codePoint == Qp) {
						if (quoteCount == 0 && build.length() > 0) {
							build.append(Q);
							throw new IllegalStateException("Quote found in unquoted value on line "
									+ lineCount + ": " + build.toString().trim());
						}

						quoteCount++;

						//Only include the quote character when find an escaped quote ("")
						if (quoteCount == 3) {
							build.append(Q);
							quoteCount -= 2;	//Reset status to 'inside quote; no other quotes seen'
						}

						//Flip quoted status
						quote = !quote;
					}

					//Delimiter - only applies when outside of quote
					else if (!quote && codePoint == delimiterP) {
						//Complete and add a value
						next.add(build.toString());
						build = new StringBuilder();
						quoteCount = 0;
					}

					//Other character
					else {
						//Detect an unescaped quote in the middle of a value
						if (quoteCount == 2) {
							throw new IllegalStateException("Unescaped quote found in quoted value on line "
									+ lineCount + ": \"" + restore(build.toString()));
						}
						build.appendCodePoint(codePoint);
					}
				}

				//Means newline was not part of value
				if (!quote) {
					next.add(build.toString());
					return prev;
				}

				//Otherwise add newline to value
				else {
					build.append('\n');
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e.getCause());
		}

		//Formatting sanity check
		if (quote) {
			throw new IllegalStateException("EOF reached while inside quoted value on line "
					+ lineCount + ": \"" + restore(build.toString()));
		}

		//Close input. Will only reach this at EOF
		try { in.close(); }
		catch (Exception e) {}

		//Ensure subsequent calls return null
		done = true;
		next = null;
		return prev;
	}


	private String restore(String out) {
		return out.trim().replaceAll(Q, QQ);
	}


	/**
	 * Call this if you're done with the input before reaching
	 * end of file to free the associated IO resources.
	 */
	public void closeEarly() {
		done = true;
		try { in.close(); }
		catch (Exception e) {}
	}


	/**
	 * Parses the first row from the given string.
	 * Meant to be used when there is only one row of data.
	 * @param csv a CSV-formatted string
	 * @return the list of values
	 */
	public static List<String> parseLine(String csv) {
		Parser parser = new Parser(csv, ',', null);
		List<String> values = parser.next();
		parser.closeEarly();
		return values;
	}


	/**
	 * Converts a string to CSV.
	 * Can handle multiple lines.
	 * @param csv a CSV-formatted string
	 * @return a list of rows parsed from the string, each with a list of values
	 */
	public static List<List<String>> fromCSV(String csv) {
		List<List<String>> lines = new ArrayList<List<String>>();
		List<String> values;
		Parser parser = new Parser(csv, ',', null);
		while ((values = parser.next()) != null) {
			lines.add(values);
		}
		return lines;
	}


	/** 
	 * Convert the provided list of values to CSV format
	 * @return the CSV-formatted string
	 */
	public static String toCSV(List<String> values) {
		return toDelimited(',', values);
	}

	public static String toDelimited(char delimiter, List<String> values) {
		final String D = "" + delimiter;
		StringBuilder build = new StringBuilder();
		boolean start = true;
		for (String s : values) {
			if (s.contains(Q) || s.contains(N) || s.contains(D)) {
				//Escape quotes
				s = s.replaceAll(Q, QQ);
				//Wrap in quotes
				s = Q + s + Q;
			}

			//Prevent comma at start of line
			if (start) start = false;
			else build.append(',');

			build.append(s);
		}
		return build.toString();
	}


	/** 
	 * Convert the provided list of values to CSV format
	 * @return the CSV-formatted string
	 */
	public static String toCSV(String... values) {
		return toCSV(Arrays.asList(values));
	}

	/** 
	 * Convert the provided list of values to the provided delimited format
	 * @return the CSV-formatted string
	 */
	public static String toDelimited(char delimiter, String... values) {
		return toDelimited(delimiter, values);
	}


	@Override
	public boolean hasNext() {
		return next != null;
	}

	@Override
	public Iterator<List<String>> iterator() {
		return this;
	}


}
