import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ImportScript {

	public static String token = "myToken";
	
	//curl -XGET --header "PRIVATE-TOKEN: XXXX" "https://gitlab.com/api/v3/projects/owned"
	public static int projectId = 1234;

	public static void main(String[] args) throws Exception {
		Parser p = new Parser("JIRA.csv");
		List<String> headers = p.next();
		int id = headers.indexOf("Issue key");
		int titlel = headers.indexOf("Summary");
		int descl = headers.indexOf("Description");
		int labell = headers.indexOf("Labels");
		int labele = headers.lastIndexOf("Labels");
		int commentl = headers.indexOf("Comment");
		int commente = headers.lastIndexOf("Comment");
		int createdl = headers.indexOf("Created");
		int status = headers.indexOf("Status");
		int priority = headers.indexOf("Priority");
//		int assignee = headers.indexOf("Assignee");

		DateFormat f = new SimpleDateFormat("MM/dd/yyyy HH:mm");

		List<List<String>> data = new ArrayList<>();
		for (List<String> row : p) data.add(row);
		data.sort((List<String> a, List<String> b) -> Integer.valueOf(a.get(id).split("-")[1]).compareTo(Integer.valueOf(b.get(id).split("-")[1])));
		int ii = 0;
		for (List<String> row : data) {
			ii++;
			int issueNumber = Integer.valueOf(row.get(id).split("-")[1]);			
			while (issueNumber > ii) {
				Issue issue = new Issue(projectId);
				issue.title = "Placeholder Issue " + ii;
				issue.addLabel("deleteMe");
				GitlabImporter.addIssue(issue, token);
				System.out.println("---Added placeholder issue " + ii + "---");
				ii++;
			}
			Issue issue = new Issue(projectId);
			issue.title = row.get(titlel);
			issue.description = row.get(descl).trim();
			if (labell >= 0) {
				for (int i = labell; i <= labele; i++) {
					String l = row.get(i);
					if (l != null && !l.isEmpty()) issue.addLabel(l);
					else break;
				}
			}
			if ("Resolved".equals(row.get(status))) issue.addLabel("Closed");
			if ("Waiting for customer".equals(row.get(status))) issue.addLabel("WaitingForCustomer");
			issue.addLabel(row.get(priority));
			for (int i = commentl; i <= commente; i++) {
				String c = row.get(i);
				if (c == null || c.isEmpty()) break;
				c = c.replaceFirst("\\;", " -- ");
				c = c.replaceFirst("\\;", "\n\n");
				issue.addComment(c.trim(), null);
			}
			issue.setCreatedAt(f.parse(row.get(createdl)));
			GitlabImporter.addIssue(issue, token);
			System.out.println("---" + ii + " done---");
		}
	}

}
