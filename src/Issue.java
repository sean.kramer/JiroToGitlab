import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class Issue {

	private static transient SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
	static { format.setTimeZone(TimeZone.getTimeZone("UTC")); }

	public Issue(int projectId) {
		id = projectId;
	}

	public void addLabel(String label) {
		if (labels == null || labels.isEmpty()) labels = label;
		else labels += "," + label;
	}

	public synchronized void setCreatedAt(Date date) { synchronized (format) {
		this.created_at = format.format(date);
	}}
	public synchronized void setDueDate(Date date) { synchronized (format) {
		this.due_date = format.format(date);
	}}

	private int id;
	public String title;
	public String description;
	public Boolean confidential;
	public Integer assignee_id;
	public Integer milestone_id;
	public String labels;
	private String created_at;
	private String due_date;
	public Integer merge_request_for_resolving_discussions;
	public Integer weight;	
	public transient int issue_id;

	public String getCreatedAt() {
		return created_at;
	}
	
	public String getDueDate() {
		return due_date;
	}
	
	private transient List<Comment> comments = new ArrayList<>();
	public void addComment(String body, Date created_at) {
		comments.add(new Comment(id, body, created_at));
	}
	public void setIssueIds(int issue_id) {
		this.issue_id = issue_id;
		for (Comment c : comments) {
			c.issue_id = issue_id;
		}
	}

	public List<Comment> getComments() { return comments; }

	public static class Comment {

		public Comment(int id, String body, Date created_at) {
			this.id = id;
			this.body = body;
			if (created_at != null) {
				synchronized (format) {				
					this.created_at = format.format(created_at);
				}
			} else {
				this.created_at = null;
			}
		}

		public final int id;
		public int issue_id;
		public final String body;
		public final String created_at;

	}

	public int getProjectId() { return id; }
}
